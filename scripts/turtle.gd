extends Node2D

onready var random_movement_timer = get_node("RandomMovementTimer")

export var destination_position = Vector2(0, 0)
export var start_position = Vector2(0, 0)
export var start_orientation = 0
export var deviation_x = 20
export var deviation_y = 20
export var num_samples_for_gaussian = 20
export var rotation_speed_rad = 1.0
export var movement_speed = 40.0 
export var random_movement = true

var position = get_pos()
var rotation = get_rot()

var rotation_direction = 1

var state = "IDLE"

func _ready():
	position_turtle(start_position)
	set_new_rotation(deg2rad(start_orientation))
	if random_movement:
		random_movement_timer.start()
	set_process(true)
	
func position_turtle(new_position):
	set_pos(new_position)
	position = get_pos()
	
func set_new_rotation(new_rotation):
	rotation = new_rotation
	state = "ROTATION" 
	if abs(get_rot() - rotation) < PI:
		rotation_direction = sign(rotation - get_rot())
	else:
		rotation_direction = sign(get_rot() - rotation)
	print(get_rot(), ' ', rotation, ' ', rotation_direction)
	
func rotate_turtle(delta):
#	print('current rot: ', get_rot(), ', destined rot: ', rotation)
	if abs(get_rot() - rotation) > rotation_speed_rad * delta * 1.1:
		set_rot(fposmod(get_rot() + rotation_speed_rad * delta * rotation_direction, 2 * PI))
	else:
		state = "MOVEMENT"
	
func _process(delta):
	if state == "ROTATION":
		rotate_turtle(delta)
	elif state == "MOVEMENT":
		move_forward(delta)
	else:
		return
	
func move_forward(delta):
#	print(Vector2(-sin(orientation), -cos(orientation)))
	var current_rotation = get_rot()
	position_turtle(position + Vector2(-sin(current_rotation), -cos(current_rotation)) * movement_speed * delta)
	
func set_destination(new_destination):
	destination_position = new_destination

func _on_random_movement_timer_timeout():
	print("Switch!")
	set_new_rotation(randf() * 2 * PI)
	random_movement_timer.set_wait_time(10 + randf() * 3)
	random_movement_timer.start()
	
func deviate_position():
	var position = get_pos()
	position += Vector2(get_gaussian(deviation_x), get_gaussian(deviation_y))
	set_pos(position)
	
func get_gaussian(deviation):
	var sum = 0.0
	for i in range(num_samples_for_gaussian):
		sum += randf()
	return (sum - num_samples_for_gaussian/2) * deviation